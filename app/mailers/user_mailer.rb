# frozen_string_literal: true

class UserMailer < ApplicationMailer
  def send_page(page)
    mail to: 'nanjel14@gmail.com', subject: 'New page saved', body: "Title:#{page.title} \n\n Content:#{page.content} "
  end
end
