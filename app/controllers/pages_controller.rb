# frozen_string_literal: true

# controller to respond to the request from view
class PagesController < ApplicationController
  def home
  end
  
  def index
    @pages = Page.order(:position)
  end

  def show
    @page = Page.find(params[:id])
  end

  def new
  @page = Page.new  
  end

  def edit
    @page = Page.find(params[:id])
  end

  def create
    @page = Page.new(page_params)
    if @page.save
      UserMailer.send_page(@page).deliver
      redirect_to @page
    else
      render 'new'
    end
  end

  def update
    @page = Page.find(params[:id])

    if @page.update(page_params)
      redirect_to pages_path
    else
      render 'edit'
    end
  end

  def destroy
    @page = Page.find(params[:id])
    @page.destroy

    redirect_to pages_path
  end

  private

  def page_params
    params.require(:page).permit(:title, :content)
  end
end
